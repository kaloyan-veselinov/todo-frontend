import React, { useState, useEffect } from 'react';
import './App.css';
import fetchTasksByParent from './services/fetchTasksByParent';
import TasksList from './components/TasksList';
import SelectedTaskPane from './components/SelectedTaskPane';

function App() {
  const [previouslyDisplayedParentTasks, setPreviouslyDisplayedParentTasks] = useState([]);
  const [displayedParentTask, setDisplayedParentTask] = useState(null);
  const [displayedTasks, setDisplayedTasks] = useState([]);
  const [selectedTask, setSelectedTask] = useState(null);

  const updateTasksOnParentUpdate = () => {
    setSelectedTask(null);
  }
  useEffect(updateTasksOnParentUpdate, [displayedParentTask]);

  const updateTasksOnSelectedTaskUpdate = () => {
    fetchTasksByParent(displayedParentTask)
      .then(tasksJson => {setDisplayedTasks(tasksJson);});
  }
  useEffect(updateTasksOnSelectedTaskUpdate, [displayedParentTask, selectedTask]);
  
  return (
    <div className="App">
      <TasksList
        displayedTasks={displayedTasks}
        setSelectedTask={setSelectedTask}
        setDisplayedParentTask={setDisplayedParentTask}
        previouslyDisplayedParentTasks={previouslyDisplayedParentTasks}
        setPreviouslyDisplayedParentTasks={setPreviouslyDisplayedParentTasks}
      />
      <SelectedTaskPane
        selectedTask={selectedTask}
        setSelectedTask={setSelectedTask}
      />
    </div>
  );
}

export default App;
