
const buildTaskQuery = id => `${process.env.REACT_APP_API_URL}/tasks/${id}/`;

const updateTask = (id, name, done, dueDate, details, priority) => fetch(buildTaskQuery(id), {
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        method: 'PATCH',                                                              
        body: JSON.stringify({
            name: name,
            done: done,
            due_date: dueDate,
            details: details,
            priority: priority
        })                                        
    })
    .then(results => results.json())

export default updateTask;