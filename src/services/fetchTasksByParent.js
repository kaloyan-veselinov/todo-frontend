import fetchCategories from './fetchCategories';

const buildTasksQuery = id => {
    const slug = `${process.env.REACT_APP_API_URL}/tasks`
    return (id == null) ? `${slug}/root/` : `${slug}/?parent=${id}`
};

const fetchTasksByParent = (parentId) => fetchCategories()
    .then(categoriesDict => fetch(buildTasksQuery(parentId))
            .then(results => results.json())
            .then(tasksJson => {
                let tasksDict = {}
                tasksJson.forEach(task => {
                    const categoryName = categoriesDict[task.category];
                    if(tasksDict.hasOwnProperty(categoryName) !== true)
                        tasksDict[categoryName] = [];
                    tasksDict[categoryName].push(task);
                })
                return tasksDict;
            })
    );

export default fetchTasksByParent;