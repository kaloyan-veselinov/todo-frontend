const buildCategoriesQuery = () => `${process.env.REACT_APP_API_URL}/categories/`;

const fetchCategories = () => fetch(buildCategoriesQuery())
    .then(results => results.json())
    .then(jsonCategories => {
        let categoriesDict = {};
        jsonCategories.forEach(category => {
            categoriesDict[category.id] = category.name;
        });
        return categoriesDict;
    });

export default fetchCategories;