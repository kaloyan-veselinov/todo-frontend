import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';


const TasksListEntry = ({task, setSelectedTask, setDisplayedParentTask, previouslyDisplayedParentTasks, setPreviouslyDisplayedParentTasks}) => {
    const selectTask = () => {setSelectedTask(task)};
    const openTask = () => {
        setDisplayedParentTask(task.id)
        setPreviouslyDisplayedParentTasks([...previouslyDisplayedParentTasks, task.parent])
    };

    const labelId = `task-list-label-${task.id}`;
    return (          
        <ListItem role={undefined} dense button onClick={selectTask}>
            <ListItemText id={labelId} primary={task.name} />
            {task.subtasks.length > 0 && 
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="Open" onClick={openTask}>
                        <ArrowForwardIosIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            }
        </ListItem>
    )
}

export default TasksListEntry;