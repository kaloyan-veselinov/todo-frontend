import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import updateTask from '../services/updateTask';

const drawerWidth = '30%';
const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex'
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      overflow: 'auto',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    field: {
        margin: "2%",
        width: "20em"
    }
  }));

const SelectedTaskPane = ({selectedTask, setSelectedTask}) => {
    const [name, setName] = useState();
    const [done, setDone] = useState();
    const [dueDate, setDueDate] = useState();
    const [details, setDetails] = useState();
    const [priority, setPriority] = useState();
    const classes = useStyles();

    const selectedTaskUpdatedEffect = () => {
        if(selectedTask != null){
            setName(selectedTask.name);
            setDone(selectedTask.done);
            setDueDate(selectedTask.due_date);
            setDetails(selectedTask.details);
            setPriority(selectedTask.priority);
        }
    }
    useEffect(selectedTaskUpdatedEffect, [selectedTask]);

    const handleNameChanged = (event) => {setName(event.target.value);};
    const handleDetailsChanged = (event) => {setDetails(event.target.value);};
    const handleDoneChanged = (event) => {setDone(event.target.checked);};
    const handleDueDateChanged = (event) => {setDueDate(event.target.value);};
    const handlePriorityChanged = (event) => {setPriority(event.target.value);};
    const submit = () => {
        updateTask(selectedTask.id, name, done, dueDate, details, priority)
            .then(updatedTask => {setSelectedTask(updatedTask);});
    };

    const getDetails = () => (details != null) ? details : "";
    const getDueDate = () => (dueDate != null) ? dueDate : "";

    return (
        <Drawer
            className={classes.drawer}
            open={selectedTask != null}
            variant="persistent"
            classes={{
            paper: classes.drawerPaper,
            }}
            anchor="right"
        >
            <Toolbar />
            <div className={classes.drawerContainer}>
                {(selectedTask != null && priority != null && name != null) ? 
                    <Grid 
                        container 
                        direction="column"
                        alignItems="center"
                        justify="center"
                    >   
                        <Grid item>
                            <FormControl className={classes.field} fullWidth>
                                <InputLabel htmlFor="task-name-field">Name</InputLabel>
                                <Input id="task-name-field" value={name} onChange={handleNameChanged}/>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControl className={classes.field} fullWidth>
                                <TextField 
                                    label="Priority"
                                    select
                                    fullWidth
                                    value={priority}
                                    onChange={handlePriorityChanged}
                                >
                                    <MenuItem value={1}>High</MenuItem>
                                    <MenuItem value={2}>Medium</MenuItem>
                                    <MenuItem value={3}>Low</MenuItem>
                                </TextField>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <TextField
                                label="Due date"
                                type="date"
                                InputLabelProps={{shrink: true,}}
                                value={getDueDate()}
                                onChange={handleDueDateChanged}
                                className={classes.field}
                            />
                        </Grid>
                        <Grid item>
                            <FormControl className={classes.field} fullWidth>
                                <InputLabel htmlFor="task-details-field">Details</InputLabel>
                                <Input id="task-details-field" value={getDetails()} multiline rows={4} onChange={handleDetailsChanged}/>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControlLabel
                                control={
                                <Checkbox
                                    checked={done}
                                    onChange={handleDoneChanged}
                                    color="primary"
                                />
                                }
                                label="Done"
                            />
                        </Grid>
                        <Grid item>
                            <Button 
                                variant="contained" 
                                color="primary"
                                onClick={submit}
                                className={classes.field}
                            >Save</Button>
                        </Grid>
                    </Grid>
                    : <React.Fragment>Click on a task to view its details...</React.Fragment>
                }
            </div>
        </Drawer>
    )
}

export default SelectedTaskPane;