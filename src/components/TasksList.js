import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import TasksListEntry from './TasksListEntry';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      justifyContent:'center',
      backgroundColor: theme.palette.background.paper,
    },
    title: {
        fontSize: 14,
    },
    card: {
        width: '68%',
        margin: '1%',
    }
}));

const TasksList = ({displayedTasks, setSelectedTask, setDisplayedParentTask, previouslyDisplayedParentTasks, setPreviouslyDisplayedParentTasks}) => {
    const classes = useStyles();

    const goBack = () => {
        let temp = [...previouslyDisplayedParentTasks];
        let previouslyDisplayedParentTask = null;
        if(temp.length > 0){
            previouslyDisplayedParentTask = temp.pop();
            setPreviouslyDisplayedParentTasks(temp);
        }
        setDisplayedParentTask(previouslyDisplayedParentTask);
    }

    return (
        <React.Fragment>
            <Toolbar>
                {previouslyDisplayedParentTasks.length > 0 && 
                    <IconButton edge="start" color="inherit" onClick={goBack}>
                        <ArrowBackIosIcon />
                    </IconButton>
                }
                <Typography variant="h6" noWrap>
                    ToDo
                </Typography>
            </Toolbar>
            {Object.keys(displayedTasks).map(categoryName =>  (
                <Card className={classes.card} key={`category_${categoryName}`}>
                    {categoryName !== 'undefined' && <CardHeader title={categoryName}/>}
                    <CardContent>
                        <List className={classes.root}>
                            {displayedTasks[categoryName].map(task => (
                                <React.Fragment key={`task_${task.id}`} >
                                    <TasksListEntry
                                        task={task}
                                        setSelectedTask={setSelectedTask}
                                        setDisplayedParentTask={setDisplayedParentTask}
                                        previouslyDisplayedParentTasks={previouslyDisplayedParentTasks}
                                        setPreviouslyDisplayedParentTasks={setPreviouslyDisplayedParentTasks}
                                    />
                                </React.Fragment>
                            ))}
                        </List>
                    </CardContent>
                </Card>
            ))}
        </React.Fragment>
    )
};

export default TasksList;